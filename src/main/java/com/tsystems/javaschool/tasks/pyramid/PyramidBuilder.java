package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        int rows = sort(inputNumbers);
        int cols = 2 * (rows - 1) + 1;  //An = 2n + 1 - последовательность для cols где n rows - 1 ;
        int[][] resMatrix = new int[rows][cols];
        return fillMatrix(inputNumbers, rows, resMatrix);
    }

    private int[][] fillMatrix(List<Integer> inputNumbers, int rows, int[][] resMatrix) {
        List<Integer> stack = new ArrayList<>();
        int offset = 0;

        for (int i = 0; i < resMatrix.length; i++) {
            int countOfNumbers = i + 1;
            for (int j = 0; j < countOfNumbers; j++) {
                stack.add(inputNumbers.get(offset + j));
            }
            offset += stack.size();

            for (int j= 0, k = 0; j < stack.size(); j++, k+=2) {
                resMatrix[i][rows-stack.size()+k] = stack.get(j);
            }

            stack.clear();
        }
        return resMatrix;
    }

    private void printMatrix(int[][] resMatrix) {
        for (int[] i : resMatrix) {
            for (int j = 0; j < i.length; j++) {
                System.out.print(i[j] + "  ");
            }

            System.out.println();
        }
    }

    private int sort(List<Integer> inputNumbers) {
        // sequence: An = 1/2 * n * (n+1);
        // Аn - дилина массива в последовательности из длин массивов,
        // с которыми можно посторить треугольник, где n - порядковый номер в этой полследовательнсоти

        // если n - не целочисленное, то Exception

        int size = inputNumbers.size();
        double n = (-1 + Math.sqrt(1 + 4 * size * 2)) / 2;

        if (n % 1 != 0) throw new CannotBuildPyramidException();

        try {
            Collections.sort(inputNumbers);
        } catch (NullPointerException e) {
            throw new CannotBuildPyramidException();
        }
        // rows == порядковому номеру в последовательности длин массива (n)
        return (int) n;
    }



}
