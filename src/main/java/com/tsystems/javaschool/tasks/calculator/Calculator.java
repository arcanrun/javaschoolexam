package com.tsystems.javaschool.tasks.calculator;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (isStatementIncorrect(statement)) return null;

        String polishStatement = getStatementByDijkstra(statement);
        if (polishStatement == null) return null;

        String evalRes =  polishEvaluate(polishStatement);
        if(evalRes == null) return null;



        double res = Double.parseDouble(evalRes);

        if (res % 1 == 0) {
            return "" + (int) res;
        } else {
            return "" + res;
        }
    }

    private String polishEvaluate(String polishStatement) {
        String[] arr = polishStatement.trim().split(" ");
        List<Double> stack = new ArrayList<>();
        for (String o : arr) {
            if (isNumber(o)) {
                try {
                    stack.add(Double.parseDouble(o));
                } catch (NumberFormatException e) {
                    return null;
                }

            } else {
                double operationRes;
                try {
                    operationRes = makeOperation(o, stack.get(stack.size() - 2), stack.get(stack.size() - 1));
                } catch (ArithmeticException e) {
                    return null;
                }
                stack.set((stack.size() - 2), operationRes);
                stack.remove(stack.size() - 1);
            }
        }


        return "" + stack.get(stack.size() - 1);
    }

    private String getStatementByDijkstra(String statement) {
        StringBuilder reversePolishNotation = new StringBuilder();
        String[] inputString = statement.split("");
        List<String> operatorStack = new ArrayList<>();

        HashMap<String, Integer> priorities = new HashMap<>();
        priorities.put("(", 1);
        priorities.put(")", 1);
        priorities.put("+", 2);
        priorities.put("-", 2);
        priorities.put("/", 3);
        priorities.put("*", 3);

        for (String o : inputString) {
            if (isNumber(o) || o.equals(".")) {
                reversePolishNotation.append(o);
            } else if (!o.equals("(") && !o.equals(")")) {
                reversePolishNotation.append(" ");
                if (operatorStack.size() > 0) {
                    while (true) {
                        int index = operatorStack.size() - 1;
                        if (priorities.get(operatorStack.get(index)) >= priorities.get(o)) {
                            reversePolishNotation.append(operatorStack.remove(operatorStack.size() - 1)).append(" ");
                            if (operatorStack.size() == 0) {

                                operatorStack.add(o);
                                break;
                            }
                        } else {

                            operatorStack.add(o);
                            break;
                        }
                    }
                } else {

                    operatorStack.add(o);
                }
            } else {
                if (o.equals("(")) {
                    operatorStack.add(o);
                } else {
                    if (operatorStack.size() == 0) return null;
                    while (!operatorStack.get(operatorStack.size() - 1).equals("(")) {
                        reversePolishNotation.append(" ").append(operatorStack.remove(operatorStack.size() - 1));
                        if (operatorStack.size() == 0) return null;
                    }
                    operatorStack.remove(operatorStack.size() - 1);
                }
            }
        }

        Iterator<String> iter = operatorStack.iterator();
        Collections.reverse(operatorStack);
        while (iter.hasNext()) {
            String elem = iter.next();
            if (elem.equals("(") || elem.equals(")")) return null;
            reversePolishNotation.append(" ").append(elem);
            iter.remove();

        }

        return reversePolishNotation.toString();
    }

    private double makeOperation(String operation, double a, double b) throws ArithmeticException {
        double res = 0.0;
        switch (operation) {
            case "+":
                res = a + b;
                break;
            case "-":
                res = a - b;
                break;
            case "*":
                res = a * b;
                break;
            case "/":
                if (b == 0) throw new ArithmeticException();
                res = a / b;

                break;
        }
        return res;
    }

    private boolean isStatementIncorrect(String statement) {
        if (statement == (null)) return true;
        if (statement.length() == 0) return true;
        if (statement.contains(",")) return true;

        Pattern pattern = Pattern.compile("[^0-9()]{2,}");
        Matcher matcher = pattern.matcher(statement);


        if (matcher.find()) return true;

        int stack = 0;
        String[] arrOfSym = statement.split("");
        for (int i = 0; i < arrOfSym.length; i++) {
            if (arrOfSym[i].equals("(")) {
                stack++;
            }
            if (arrOfSym[i].equals(")")) {
                stack--;
            }
            if (stack < 0) {
                return true;
            }

        }
        if (stack > 0) return true;
        return false;
    }

    private boolean isNumber(String token) {
        try {

            Double i = Double.parseDouble(token);
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }

}
